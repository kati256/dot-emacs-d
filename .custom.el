(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-enabled-themes (quote (rebecca)))
 '(custom-safe-themes
   (quote
    ("f633d825e380caaaefca46483f7243ae9a663f6df66c5fad66d4cab91f731c86" default)))
 '(package-selected-packages
   (quote
    (rebecca-theme company-c-headers ggtags rust-mode go-eldoc flymake-go company-go go-mode pyenv-mode py-autopep8 pip-requirements elpy yasnippet wgrep which-key undo-tree smex smartparens org-bullets org-projectile neotree multiple-cursors magit hlinum counsel-projectile counsel flycheck expand-region exec-path-from-shell dashboard company avy spacemacs-theme use-package)))
 '(pdf-view-midnight-colors (quote ("#b2b2b2" . "#292b2e"))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
