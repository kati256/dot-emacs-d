;;; -*- no-byte-compile: t -*-
(define-package "company-go" "20180427.1856" "company-mode backend for Go (using gocode)" '((company "0.8.0") (go-mode "1.0.0")) :commit "dc34cac77b4c9ddb478256443f797d5e6df2d93c" :keywords '("languages") :authors '(("nsf" . "no.smile.face@gmail.com")) :maintainer '("nsf" . "no.smile.face@gmail.com"))
