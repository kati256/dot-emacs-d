;;; -*- no-byte-compile: t -*-
(define-package "swiper" "20181120.1832" "Isearch with an overview. Oh, man!" '((emacs "24.1") (ivy "0.9.0")) :commit "be62a3798c3eef64a4ebf6d30dea1db93bf9857c" :keywords '("matching") :authors '(("Oleh Krehel" . "ohwoeowho@gmail.com")) :maintainer '("Oleh Krehel" . "ohwoeowho@gmail.com") :url "https://github.com/abo-abo/swiper")
